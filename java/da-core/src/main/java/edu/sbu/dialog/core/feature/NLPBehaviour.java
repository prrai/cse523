package edu.sbu.dialog.core.feature;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.sbu.dialog.core.dialogmanager.Context;
import edu.sbu.dialog.core.utilities.Constants;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

/**
 * This class performs the action of extracting the POS from the user speech
 * @author Kedar
 *
 */
public class NLPBehaviour implements Behaviour<String,Map<String,List<String>>> {

	 
	List<String> requiredTagList=Arrays.asList(new String[] {"NN","CD","RB","DT"});
	public Map<String, List<String>> doAction(Context context,String input) {
		Map<String, List<String>> posMap=extractPOSMap(input);
		return posMap;
	}

	
	private Map<String,List<String>> extractPOSMap(String text) {
		MaxentTagger m = new MaxentTagger(Constants.TAGGER_PATH+"/english-left3words-distsim.tagger"); //"src/main/resources/english-left3words-distsim.tagger");
		String[] tokens = m.tagTokenizedString(text).split(" ");
		List<String> nouns=new ArrayList<String>();
		
		Map<String,List<String>> outputMap=new HashMap<String, List<String>>();
		for(String token:tokens) {
			System.out.println(token);
			String words[]=token.split("_");
			if(words!=null&&words.length==2) {
				if(requiredTagList.contains(words[1])) {
					List<String> tokenList=outputMap.get(words[1]);
					if(tokenList==null){
						tokenList=new ArrayList<String>();
						outputMap.put(words[1],tokenList);
					}
					tokenList.add(words[0]);
				}
			}
				
		}
		System.out.println(outputMap);
		return outputMap;
	}

	

	
	
	

}
