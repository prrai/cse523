package edu.sbu.dialog.core.dialogmanager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.sbu.dialog.core.feature.Behaviour;
import edu.sbu.dialog.core.feature.FilterBehaviour;
import edu.sbu.dialog.core.feature.FoodTokenExtractionBehaviour;
import edu.sbu.dialog.core.feature.NLPBehaviour;
import edu.sbu.dialog.core.pojos.State;
import edu.sbu.dialog.core.pojos.StateEnum;
import edu.sbu.dialog.core.taskmanager.TaskManager;
import edu.sbu.dialog.core.utilities.Constants;
import edu.sbu.dialog.core.utilities.Messages;
import edu.sbu.dialog.core.utilities.POSTags;

/**
 * this class implements the dialog system using concept of finite state automaton.
 *  The user response transitions the system from one state to another. 
 * @author kedar
 *
 */
public class DialogContext implements Context{
	private State currentState,previousState,nextState;
	private static TaskManager taskManager;
	private String textFromUser;
	private String textToUser;
	private static Map<StateEnum,State> stateMap;
	//private State[][] transitionTable;
	private List<String> choices; 
	
	static{
		taskManager=new TaskManager();
		stateMap=new HashMap<StateEnum,State>();
		stateMap.put(StateEnum.INITIAL, new State(StateEnum.INITIAL));
		stateMap.put(StateEnum.FILTER, new State<List<String>,List<String>>(StateEnum.FILTER,new FilterBehaviour(taskManager)));
		stateMap.put(StateEnum.INFORM_NUTRIENTS, new State(StateEnum.INFORM_NUTRIENTS));
		stateMap.put(StateEnum.NLP, new State<String,Map<String,List<String>>>(StateEnum.NLP,new NLPBehaviour()));
		stateMap.put(StateEnum.FOODITEM_EXTRACTION, new State<Map<String,List<String>>, List<String>>(StateEnum.FOODITEM_EXTRACTION,new FoodTokenExtractionBehaviour(taskManager)));
		stateMap.put(StateEnum.CONTINUE, new State(StateEnum.CONTINUE));
		//stateMap.put(StateEnum.GREET, new State(StateEnum.GREET));
	}
	
	/**
	 * This method receives user input as text . it sets the textFromUser variable and calls applytransitionlogic
	 * @param text
	 * @return
	 */
	public String processRequest(String text) {
		this.textFromUser=text;
		applyTransitionLogic();
		return textToUser;
	}
	
	/**
	 * This method holds all the logic of transitioning between states based on user input and previous state
	 * 
	 * The logic starts with initial state being null or INITIAL. When user responds , the input transitions 
	 * the context to nlp state where it breaks down the user response into nouns, verbs, etc
	 * 
	 *  then the system moves to food extraction  state where it identifies the food items in the response
	 *  then based on previous state and the user's response decides the next possible state. Each state is 
	 *  associated with a state behaviour. When system moves to a state , its behaviour is invoked.
	 */
	private void applyTransitionLogic() {
		//BaseVO data=null;
		State state=stateMap.get(StateEnum.NLP);
		Behaviour<String,Map<String,List<String>>> behaviour=state.getBehaviour();
		Map<String,List<String>> posMap=behaviour.doAction(this,textFromUser);
		//System.out.println(list.getList());
		List<String> tokens=null;
		if((previousState==null||previousState.getState()==StateEnum.INITIAL||previousState.getState()==StateEnum.FILTER)) {
			
			state=stateMap.get(StateEnum.FOODITEM_EXTRACTION);
			Behaviour<Map<String,List<String>>,List<String>> foodExtractionBehaviour=state.getBehaviour();
			tokens=foodExtractionBehaviour.doAction(this,posMap);
		}
		if(previousState!=null&&previousState.getState()==StateEnum.CONTINUE )/*&& (posMap.get(POSTags.RB)!=null||posMap.get(POSTags.DT)!=null)) */{
			
			taskManager.initializeFilterList();
			choices=null;
			
			List<String> responses=posMap.get(POSTags.RB);
			if(responses==null)
				responses=posMap.get(POSTags.DT);
			
			String response="yes";
			if(responses!=null)
				response=responses.get(0);
			if("no".equalsIgnoreCase(response)) {
				
				textToUser=Messages.GREET;
				currentState=null;
			}else if("yes".equalsIgnoreCase(response)) {
			currentState=stateMap.get(StateEnum.INITIAL);
			
			textToUser=Messages.ASK_CLIENT;
			}
		}
		if((previousState==null && (tokens==null|| tokens.size()==0))|| (previousState!=null && previousState.getState()==StateEnum.INFORM_NUTRIENTS)) {
			
			currentState=stateMap.get(StateEnum.INITIAL);
			taskManager.initializeFilterList();
			choices=null;
			textToUser=Messages.ASK_CLIENT;	
		}
		if(previousState!=null&&(previousState.getState()==StateEnum.INITIAL||previousState.getState()==StateEnum.FILTER )&& tokens!=null&& tokens.size()==0){
			
			currentState=previousState;
			textToUser=Messages.REPEAT;
		}		
		
		
		if((previousState==null||previousState.getState()==StateEnum.INITIAL) && tokens!=null&& tokens.size()>0 ||
				(previousState!=null &&previousState.getState()==StateEnum.FILTER && tokens!=null&& tokens.size()>0&& choices!=null&&choices.size()>3) ) {
			{
						
						currentState=stateMap.get(StateEnum.FILTER); 
						textToUser=Messages.SELECT_CHOICE;
						Behaviour<List<String>,List<String>> filterBehaviour=currentState.getBehaviour();
						choices= filterBehaviour.doAction(this, tokens);
						
						if(choices.size()<3)
						{
							StringBuilder sb=new StringBuilder("Say ");
							String[] ch= {"ONE for {1}","TWO for {2}"};
							for(int i=0;i<choices.size();i++) {
								sb.append(ch[i].replace("{"+(i+1)+"}", choices.get(i)));
								sb.append(",");
							}
							textToUser=sb.toString().substring(0,sb.toString().length()-1);
						} 
						
						else
							for(int i=0;i<Constants.NUMBER_OF_CHOICES;i++) {
								textToUser=textToUser.replace("{"+(i+1)+"}", choices.get(i));
							}
						
						tokens=null;
			 }
		}
		
		if(previousState!=null&&previousState.getState()==StateEnum.FILTER && tokens!=null&& tokens.size()>0&&choices!=null&& choices.size()<=3) {
			
			currentState=stateMap.get(StateEnum.INFORM_NUTRIENTS);
			String choice=choices.get(Constants.choices.get(tokens.get(0))-1);
			String calories=taskManager.getCalories(choice);
			textToUser=Messages.INFORM_NUTRIENTS;
			textToUser=textToUser.replace("{1}", choice).replace("{2}", calories);
			choices=null;
			currentState=stateMap.get(StateEnum.CONTINUE);
			//System.out.println("Current state ="+currentState);
		}		
		previousState=currentState;
	}

	public State getCurrentState() {
		return currentState;
	}
}
