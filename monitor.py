import os
import subprocess
cmd = 'python script.py'

if __name__ == '__main__':
    start_ctr = 0
    while True:
        with open('start_ctr.txt', 'r') as data:
            start_ctr = int(data.read())
            print("Reading from file: {}".format(start_ctr))
            data.close()
        start_ctr -= 1
        print("Run from: {}".format(start_ctr))
        cmd = 'python3.5 DB.py {}'.format(start_ctr)
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        out, err = p.communicate()
        rc = p.returncode
        if rc == 0:
            exit()
